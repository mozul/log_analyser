
import re
from collections import defaultdict

NTEST = 3

def readFile(input_file, debug=False):
  """
  read a log file to get ip accessing the server
  """

  pattern = re.compile(r"\A(?P<ip>(\d{1,3}\.?){4}(?P<proto>(GET|POST))")

  list_ip  = []
  with open(input_file, 'rt', encoding="utf8") as file_in:
      i_line = 0
      for line in file_in:
          found = pattern.match(line)
          if found:
              list_ip.append( found['ip'], found['proto'] )

          # temporary to check the regex
          i_line += 1
          if debug and i_line >= debug:
              break

  return list_ip


def countIp( list_ip ):
  """
  make a dictionnary of ip and number of
  occurences
  """

  count_ip = defaultdict(int)
  for ip in list_ip:
    #if ip not in count_ip.keys():
    #  count_ip[ip] = 0
    count_ip[ip] += 1

  return count_ip

def countProto( list_ip, ip ):
   """
   count occurence of each proto for an ip
   """

   get = 0
   post = 0
   for ip_loc in list_ip:

     if ip_loc[0] == ip:

       if ip_loc[1] == 'GET':
         get += 1
       else :
         post += 1

   return get, post


#l_ip = readFile('gitlab_access.log',debug=5)
l_ip = readFile('gitlab_access.log')
count = countIp(l_ip)
print( f"number of lines   : {len(l_ip)}" )
print( f"number of ip      : {len(count)}" )
print( f"max number of oc. : {max(count.values())}" )
print( f"sum of occurences: {sum(count.values())}" )

